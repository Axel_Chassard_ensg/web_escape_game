<?php
  echo "<p>Bravo, vous avez finis cette escape game</p>";

  //connexion à la bdd
  include('./connect.php');
  mysqli_set_charset($link, "utf8");

  //requête de récupération de l'id de la partie
  $requete1 = "SELECT idPartie AS idP FROM partie ORDER BY idPartie DESC LIMIT 1";

  $result1 = mysqli_query($link,$requete1);
  $ligne1 = mysqli_fetch_assoc($result1);
  $idPartie = $ligne1["idP"];


  //requête de récupération du $score
  $requete2 = "SELECT score FROM partie WHERE idPartie = $idPartie";

  $result2 = mysqli_query($link,$requete2);
  $ligne2 = mysqli_fetch_assoc($result2);
  $score = $ligne2["score"];

  //créer une vue ressemblant à cela
  //(SELECT ROW_NUMBER() OVER(ORDER BY score DESC) AS row,idPartie) AS sr

  //requete permettant de récupérer le classement
  // $requete3 = "SELECT row FROM view WHERE idPartie = $idPartie";

  $requete3 = "SELECT COUNT(*) AS nb FROM partie WHERE score>$score";

  $result3 = mysqli_query($link,$requete3);
  $ligne3 = mysqli_fetch_assoc($result3);
  $classement = $ligne3["nb"]+1;



 ?>

<!DOCTYPE html>
<html lang=fr dir="ltr">
  <head>
    <meta charset="utf-8">
    <link href="../../public/bootstrap/bootstrap-4.4.1/dist/css/bootstrap.css" rel="stylesheet">
    <link rel="icon" type="image/png" href="../../public/img/background/logo-noir.gif" />
    <title></title>
  </head>
  <body>
    <div class="">
      <?php
        echo "<p> id partie : $idPartie</p>";
        echo "<p>Vous avez réalisé un score de $score </p>";
        echo "<p>Votre score est classé numéro $classement de tous les temps</p>";
        echo "<img src=../../public/img/background/neville.gif>"
       ?>
    </div>

    <form action="./viderSession.php" method="post">
      <input type="submit" class="btn" value="Accueil">
    </form>
    <script src ="../../public/jquery/jquery-3.4.1.js" ></script>
    <script src="../../public/bootstrap/bootstrap-4.4.1/dist/js/bootstrap.bundle.min.js"></script>

  </body>
</html>
