<!-- script ayant pour but de recuperer les donnees saisies par l'utilisateur lors de l inscription et de les verifier avec la bases de donnees pour ensuite rediriger vers la bonne page -->
<?php
include('./connect.php');

session_start();

$form = true;
$message = "p";
$username = "";
$pass = "";
$email = "";
$id = "15";

// verif si username bien saisie
if (isset($_POST["username"])){
  $username = $_POST["username"];
  $username = strtolower($username);
  if (empty($_POST["username"])){
    $message ="username non renseigné";
    $form = false;
  }
}

// verif si password bien saisie
if (isset($_POST["password"])){
  $pass = $_POST["password"];
  if (empty($_POST["password"])){
    $message ="password non renseigné";
    $form = false;
  }
}


$exp_reg_mail = " /^[^\W][a-zA-Z0-9_]+(\.[a-zA-Z0-9_]+)*\@[a-zA-Z0-9_]+(\.[a-zA-Z0-9_]+)*\.[a-zA-Z]{2,4}$/ ";
// verfi si email bien saisie
if (isset($_POST["email"])){
  $email = $_POST["email"];
  if (empty($_POST["email"])){
    $message ="email non renseigné";
    $form = false;
  }
  if (!preg_match($exp_reg_mail,$email)){
    $message = "email incorrect";
    $form = false;
  }
}

if ($form){
  // echo "$username $pass $email";
  $requete_pseudo = 'SELECT * FROM utilisateur WHERE pseudo = "'.$username.'"';
  // echo $requete_pseudo;
  $requete_mail = 'SELECT * FROM utilisateur WHERE email = "'.$email.'"';
  $requete_inser = 'INSERT INTO utilisateur (pseudo,pswd,email) VALUES ("'.$username.'","'.$pass.'","'.$email.'")';
  // echo $requete_inser;
  // $requete_inser = "INSERT INTO utilisateur (pseudo,pswd,email) VALUES ('Alex','alex','alex@gmail.com')";
  $c=0;
  if ($result1 = mysqli_query($link,$requete_pseudo)){
    while($ligne = mysqli_fetch_assoc($result1)){
      $c+=1;
    }
    if ($c != 0){
      $form = false;
      $message = "username deja pris";
    }
  }

  if ($result2 = mysqli_query($link,$requete_mail)){
    while($ligne = mysqli_fetch_assoc($result2)){
      $c+=1;
    }
    if ($c != 0){
      $form = false;
      $message = "email deja utilise pour un autre compte";
    }
  }
  if ($c==0){
    // echo "$requete_inser";
    $result3 = mysqli_query($link,$requete_inser);
    $_SESSION["username"]=$login;
    $_SESSION["active"]="oui";
  }
}

mysqli_close($link);
// chargement de la page d'identification si form ok
if ($form){
  // echo $message;
  header('Location: ../../public/index.php');
}
// chargement de la page d inscription si form pas ok
else{
  // echo "$message";
  header('Location: ../html/sign_in.html');
}
?>
