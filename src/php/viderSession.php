<?php

  session_start();
  foreach ($_SESSION as $key =>$value){
    if ($key!="username" && $key!="active"){
      unset($_SESSION[$key]);
    }
  }

  header('Location: ../../public/index.php');

?>
