<!-- script ayant pour but de recuperer les donnees saisies par l'utilisateur lors de l authentification et de les verifier avec la bases de donnees pour ensuite rediriger vers la bonne page -->
<?php
// connexion a la base de donnees
include('./connect.php');

session_start();

$form=true;
//$message="";

// verif si login bien saisie
if (isset($_POST["login"])){
  $login = $_POST["login"];
  $login = strtolower($login);
  if (empty($_POST["login"])){
    //$message ="login non renseigné";
    $form = false;
  }
}

// verif si password bien saisie
if (isset($_POST["password"])){
  $pass = $_POST["password"];
  if (empty($_POST["password"])){
    //$message ="password non renseigné";
    $form = false;
  }
}

// if ((!empty($_POST["login"])) && (!empty($_POST["password"]))){
if ($form){
  // code avec database
  $requete = 'SELECT * FROM utilisateur WHERE pseudo="'.$login.'"';
  if ($result = mysqli_query($link,$requete)){
    $ligne = mysqli_fetch_assoc($result);
    if ($ligne["pswd"]!=$pass){
      $form = false;
      $login = "";
      $pass = "";

    }
    else{
      $_SESSION["username"]=$login;
      $_SESSION["active"]="oui";
    }
  }
}



mysqli_close($link);

// chargement de la page d'authentification si form pas ok
if ($form == false){
  header('Location: ../html/log_in.html');
}

// chargement de la page de jeu si form ok
else{
  header('Location: ../../public/index.php');
}

?>
