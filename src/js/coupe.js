
//TODO :

//Revenir sur la duplication retardée avec le setTimeout : trouver comment empêcher l'affichage non désiré des images retardée après le gain du jeu

// import {infanticide} from './module.js';


//***********************************************************************************************************
//fonction
//***********************************************************************************************************

function chargerSession(){
  fetch("../php/session.php")
  .then(r=>r.json())
  .then(r=>{
    for (let nom in r){
      if (nom=="debut"){
        temps=r['debut'];
      }
    }
    if (temps==null){
      initMinuteur();
    }
  })
  .then(afficheMinuteur)
}


function initMinuteur(){
  var data = new FormData();
  var debut = new Date();
  temps=debut.getTime();
  data.append("debut",temps);
  fetch("../php/majSession.php",{
    method : 'post',
    body : data
  })
}


function afficheMinuteur(){
  // console.log("affcihe");
  // console.log(temps);
  var date = new Date();
  var min = document.getElementById("minuteur");
  if (temps!=null){
    var tps = 1200000-(date.getTime()-parseInt(temps));
    // console.log(tps);
    if (tps>=0){
      var tps_secondes = tps / 1000;
      var minutes = Math.floor(tps_secondes / 60);
      var secondes = Math.round((tps_secondes % 60)*1000)/1000;
      min.innerHTML=minutes + ":"+secondes;
      setTimeout(afficheMinuteur,10);
    }
    else{
      // majSession("temps-restant",0);
      var data = new FormData();
      data.append("temps-restant",0);

      fetch("../php/majSession.php",{
        method : 'post',
        body : data
      })
      .then(fetch("../php/fin.php")
      // .then(r=>r.text())
      // .then(r=>console.log("r : "+r))
      .then(window.location.href = "../html/voldemort.html"))
    }

  }

}


function majSession(nom,valeur){
  // console.log('maj session');
  // console.log(nom);
  var data = new FormData();
  data.append(nom,valeur);

  fetch("../php/majSession.php",{
    method : 'post',
    body : data
  })
  // .then(r=>console.log("majSession"))
}


function infanticide(pere){
  // fontion permettant de supprimer tous les enfants d'une balise html
  //
  // param pere : balise html dont on veut supprimer les enfants
  // type pere : html object
  // console.log("infanticide");
  while(pere.firstElementChild) {
    pere.removeChild(pere.firstElementChild);
  }
}


function creer_img(poufsouffle,numero){
  ///Fonction permettant d'ajouter une balise image à la page, placé aléatoirement.
  ///
  ///param poufsouffle : indique si l'image est la coupe de poufsouffle.
  ///type poufsouffle : boolean
  ///param numero : numéro de l'image qui permet de savoir laquelle dupliquer
  ///type numero : entier
  ///return : l'image créé
  ///rtype : html object <img>

  image = document.createElement("img");
  // div_jeu = document.getElementById("jeu");
  // div_jeu.appendChild(image);
  document.body.append(image);
  if(poufsouffle){
    image.className = "vrai_coupe";
    image.src = "../../public/img/coupe/poufsouffle.png";
  }
  else{
    image.className = "fausse_coupe";
    image.src = "../../public/img/coupe/coupe" + numero + ".png";
  }

  image.id = numero;
  image.style.width = window.innerWidth/8 + "px";
  image.style.height = "auto";

  image.addEventListener("click",action_click);
  image.addEventListener("mouseover",function(e){
    e.target.style.cursor = "pointer";
  })
  image.addEventListener("load",function(e){
    placer_image_aleatoirement(e.target);
  })

  return image;
}


function placer_image_aleatoirement(image){
  ///Fonction permettant de placer une image aléatoirement dans la page
  ///
  ///param image : l'image à placer aléatoirement
  //type image : html object <img>
  //no return

  image.style.position = "absolute";
  image.style.left = Math.random()*(window.innerWidth-image.width) + "px";
  image.style.top = Math.random()*(window.innerHeight-image.height-hauteur_p) + hauteur_p + "px";
}


function action_click(event){
  ///Fonction qui va déclencher l'affichage du gain si l'image cliqué est la bonne,
  ///ou va dupliquer l'image cliqué si ce n'est pas la bonne
  ///
  ///param event : l'événement déclenché au clic sur une image
  ///type event : événement
  ///no return

  if(event.target.className == "vrai_coupe"){
        afficher_gain();
      }
  else{
        dupliquer(event.target);
        }
}


function dupliquer(image){
  ///Fonction permettant dupliquer une image tout en la positionnement aléatoirement, elle et sa réplique
  ///
  ///param image : l'image à dupliquer
  ///type image : html object <img>
  ///no return


  placer_image_aleatoirement(image);
  tab[tab.length] = creer_img(false,image.id);

  }


function loop(sec){
  ///Fonction permettant de dupliquer une image aléatoire au bout d'un certain nombre de secondes
  ///
  ///param sec : nombre de secondes au bout duquel on souahite dupliquer une image
  ///type sec : entier
  ///no return

  ///permet de stopper la duplication lorsque la bonne coupe est trouvée
  if(trouve){
    // console.log("trouvé : " + trouve);
    return;
  }
  indice = Math.trunc(Math.random()*(tab.length-2))+1;
  // console.log("tab[indice]"+tab[indice]);
  dupliquer(tab[indice]);
  setTimeout(loop,sec*1000);
}

// function infanticide(pere){
//   // fontion permettant de supprimer tous les enfants d'une balise html
//   //
//   // param pere : balise html dont on veut supprimer les enfants
//   // type pere : html object
//
//
//
//   while(pere.firstElementChild) {
//     pere.removeChild(pere.firstElementChild);
//   }
// }

function afficher_gain(){
  ///Fonction permettant de vider la fenêtre et d'afficher un message lorsque la coupe de Poufssoufle est retrouvé

  window.location.href ="../html/gain_coupe.html";


}



function resize(){
  //fonction permettant de redimensionner les images de l'ecran si celui ci est amener a s'agrnadir ou retrecir

  var images = document.body.querySelectorAll("img");
  // console.log(images.length);
  for (let img in images){
    // console.log(images[img].style);
    if (images[img].style != undefined){
      images[img].style.width = window.innerWidth/8 + "px";
      images[img].style.height="auto";
      images[img].style.left = Math.random()*(window.innerWidth-image.width) + "px";
      images[img].style.top = Math.random()*(window.innerHeight-image.height-hauteur_p) + hauteur_p + "px";
    }
  }

}

//***********************************************************************************************************
//exécution principale
//***********************************************************************************************************

///Variables globals
var x;
var y;

var temps=null;


//On récupère la hauteur du premier paragraphe afin, par la suite, de générer aléatoirement des images sous
//ce paragraphe
var p = document.querySelector("p");//***********************************************************************************************************
//Fonction
//***********************************************************************************************************

function chargerSession(){
  fetch("../php/session.php")
  .then(r=>r.json())
  .then(r=>{
    for (let nom in r){
      if (nom=="debut"){
        temps=r['debut'];
      }
    }
    if (temps==null){
      initMinuteur();
    }
  })
  .then(afficheMinuteur)
}


function initMinuteur(){
  var data = new FormData();
  var debut = new Date();
  temps=debut.getTime();
  data.append("debut",temps);
  fetch("../php/majSession.php",{
    method : 'post',
    body : data
  })
}


function afficheMinuteur(){
  // console.log("affcihe");
  // console.log(temps);
  var date = new Date();
  var min = document.getElementById("minuteur");
  if (temps!=null){
    var tps = 1200000-(date.getTime()-parseInt(temps));
    // console.log(tps);
    if (tps>=0){
      var tps_secondes = tps / 1000;
      var minutes = Math.floor(tps_secondes / 60);
      var secondes = Math.round((tps_secondes % 60)*1000)/1000;
      min.innerHTML=minutes + ":"+secondes;
      setTimeout(afficheMinuteur,10);
    }
    else{
      // majSession("temps-restant",0);
      var data = new FormData();
      data.append("temps-restant",0);

      fetch("../php/majSession.php",{
        method : 'post',
        body : data
      })
      .then(fetch("../php/fin.php")
      // .then(r=>r.text())
      // .then(r=>console.log("r : "+r))
      .then(window.location.href = "../html/voldemort.html"))
    }

  }

}


function majSession(nom,valeur){
  // console.log('maj session');
  // console.log(nom);
  var data = new FormData();
  data.append(nom,valeur);

  fetch("../php/majSession.php",{
    method : 'post',
    body : data
  })
  // .then(r=>console.log("majSession"))
}


function creer_img(poufsouffle,numero){
  ///Fonction permettant d'ajouter une balise image à la page, placé aléatoirement.
  ///
  ///param poufsouffle : indique si l'image est la coupe de poufsouffle.
  ///type poufsouffle : boolean
  ///param numero : numéro de l'image, permet de savoir laquelle dupliquer
  ///type numero : entier
  ///return : l'image créé
  ///rtype : html balise <img>


  image = document.createElement("img");
  document.body.append(image);

  //On teste si la coupe est celle de Poufsouffle, si c'est le cas on lui attribue la classe vrai_coupe qu'elle seule possède,
  //sinon, on charge une image du répertoire des coupes et on lui attribue la classe "fausse_coupe"
  if(poufsouffle){
    image.className = "vrai_coupe";
    image.src = "../../public/img/coupe/poufsouffle.png";
  }
  else{
    image.className = numero;
    image.src = "../../public/img/coupe/coupe" + numero + ".png";
  }

  //On définit une largeur et une hauteur responsive à la largeur de la fenêtre
  image.style.width = window.innerWidth/8 + "px";
  image.style.height = "auto";

  image.addEventListener("click",action_click);
  image.addEventListener("mouseover",function(e){
    e.target.style.cursor = "pointer";
  })
  placer_image_aleatoirement(image);


  return image;
}


function placer_image_aleatoirement(image){
  ///Fonction permettant de placer une image aléatoirement dans la page
  ///
  ///param image : l'image à placer aléatoirement
  //type image : html balise <img>
  //no return

  //On place l'image aléatoirement en s'assurant qu'elle ne dépasse entièrement les dimensions de la fenêtre
  image.style.position = "absolute";
  image.style.left = Math.random()*(window.innerWidth-image.width) + "px";
  image.style.top = Math.random()*(3*window.innerHeight/4-hauteur_p) + hauteur_p + "px";
}


function action_click(event){
  ///Fonction qui va déclencher l'affichage du gain si l'image cliqué est la bonne,
  ///ou va dupliquer l'image cliqué si ce n'est pas la bonne
  ///
  ///param event : l'événement déclenché au clic sur une image
  ///type event : événement
  ///no return

  if(event.target.className == "vrai_coupe"){
        afficher_gain();
      }
  else{
        dupliquer(event.target);
        }
}


function dupliquer(image){
  ///Fonction permettant de dupliquer une image tout en la positionnant aléatoirement, elle et sa réplique
  ///
  ///param image : l'image à dupliquer
  ///type image : html balise <img>
  ///no return

  //On commence par positionner aléatoirement celle cliqué
  placer_image_aleatoirement(image);

  //Puis on ajoute la même image à la page
  tab[tab.length] = creer_img(false,image.className);

  }


function loop(){
  ///Fonction permettant de dupliquer une image aléatoirement toutes les deux secondes
  ///
  ///no return

  //On récupère une image aléatoirement dans celle de la page et on la duplique
  indice = Math.trunc(Math.random()*((tab.length-2)))+1;
  dupliquer(tab[indice]);
  setTimeout(loop,2000);
}


function afficher_gain(){
  ///Fonction permettant d'ouvrir la page de gain lorsque la coupe de Poufssouffle est trouvé

  window.location.href ="../html/gain_coupe.html";


}


function resize(){
  //Fonction permettant de redimensionner les images de la fenêtre si celle ci est amener à changer de dimension
  ///
  ///no return

  var images = document.body.querySelectorAll("img");
  for (let img in images){
    if (images[img].style != undefined){
      images[img].style.width = window.innerWidth/8 + "px";
      images[img].style.height="auto";
      placer_image_aleatoirement(images[img]);
    }
  }

}

//***********************************************************************************************************
//exécution principale
//***********************************************************************************************************


//On récupère la hauteur du premier paragraphe afin, par la suite, de générer aléatoirement des images sous
//ce paragraphe
var p = document.querySelector("p");
var hauteur_p = p.offsetHeight;

//on ajoute à la console l'image de la coupe de Poufsouffle. On la stocke en même temps dans une variable
var poufsouffle = creer_img(true,0);

//on créé à la vollée des balises images que l'on remplit avec les coupes et que l'on place aléatoirement
var tab = [];
tab[0] = poufsouffle;
for (var i = 1; i < 10; i++) {
  tab[i] = creer_img(false,i);
}


chargerSession();
afficheMinuteur();

//on duplique des éléments toutes les 5 secondes pour rajouter de la difficulté au jeu
setTimeout(loop,2000);


//Si l'utilisateur va varrier la taille de la fenêtre, les positions sont regénérées en fonction de la nouvelle taille
window.onresize=resize;

var hauteur_p = p.offsetHeight;

//on ajoute à la console l'image de la coupe de Poufsouffle. On la stocke en même temps dans une variable
var poufsouffle = creer_img(true,0);

//on créé à la vollée des balises images que l'on remplit avec les coupes et que l'on place aléatoirement
var tab = [];
tab[0] = poufsouffle;
for (var i = 1; i < 10; i++) {
  tab[i] = creer_img(false,i);
}

//on créé une variable booléenne qui serait vrai quand la coupe sera trouvée
var trouve = false;

chargerSession();
afficheMinuteur();

//on duplique des éléments toutes les 5 secondes pour rajouter de la difficulté au jeu
//setTimeout(loop(5),5000);



window.onresize=resize;
