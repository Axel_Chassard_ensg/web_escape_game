//TODO :
// trouver comment récupérer la réussite d'un mini-jeu pour débloquer les horcruxes bloqués à ce moment-là

//******************************************************************************
//Import
//******************************************************************************

import {encode_lettre} from './code_lettre.js';
import {decode_lettre} from './code_lettre.js';


//******************************************************************************
//Classe
//******************************************************************************

class Figurine{
  nom;
  lat;
  long;
  url;
  horcruxe;  //boolean indiquant si la figure est un horcruxe ou non
  bloque;    //boolean indiquant si l'image est bloqué ou non
  ouverture;  //page ouverte par certains marqueurs, si le marqueur n'ouvre pas de page, l'url est à ""
  detruit;  //boolean indiquant si la figure a ete detruite ou non
  clef; //string correspondant au nom de la figure pouvant le debloque

  constructor(nom,lat,long,url,horcruxe,bloque,ouverture,detruit,clef){
    this.nom = nom;
    this.lat = lat;
    this.long = long;
    this.url = url;
    this.horcruxe = horcruxe;
    this.bloque = bloque;
    this.ouverture = ouverture;
    this.detruit=detruit;
    this.clef=clef;
  }
}

//******************************************************************************
//Fonction
//******************************************************************************

//fonction qui permet de faire un modulo d'un nombre negatif
// car % d'un nombre negatif renvoie un modula negatif
Number.prototype.mod = function(n) {
  var m = (( this % n) + n) % n;
  return m < 0 ? m + Math.abs(n) : m;
};

//****************************************************************************************************************
//Ajout Alex
//****************************************************************************************************************





function chargerSession(){
  fetch("../php/session.php")
  .then(r=>r.json())
  .then(r=>{
    for (let nom in r){
      if (nom=="debut"){
        temps=r['debut'];
      }
      if (nom=="temps_harry"){
        temps_harry=r['temps_harry'];
      }
    }
    if (temps==null){
      initMinuteur();
    }
  })
  .then(afficheMinuteur)
}

function initMinuteur(){
  var data = new FormData();
  var debut = new Date();
  temps=debut.getTime();
  data.append("debut",temps);
  fetch("../php/majSession.php",{
    method : 'post',
    body : data
  })
}

function afficheMinuteur(){
  // console.log("affcihe");
  // console.log(temps);
  var date = new Date();
  var min = document.getElementById("minuteur");
  if (temps!=null){
    if (temps_harry==null){
      var tps = 1200000-(date.getTime()-parseInt(temps));
    }
    else{
      var tps = 60000-(date.getTime()-parseInt(temps_harry));
    }

    // console.log(tps);
    if (tps>=0){
      var tps_secondes = tps / 1000;
      var minutes = Math.floor(tps_secondes / 60);
      var secondes = Math.round((tps_secondes % 60)*1000)/1000;
      min.innerHTML=minutes + ":"+secondes;
      setTimeout(afficheMinuteur,10);
    }
    else{
      // majSession("temps-restant",0)
      // .then(fetch("../php/fin.php")
      // // .then(r=>r.text())
      // .then(window.location.href = "../html/voldemort.html"))
      var data = new FormData();
      data.append("temps-restant",0);

      fetch("../php/majSession.php",{
        method : 'post',
        body : data
      })
      .then(fetch("../php/fin.php")
      // .then(r=>r.text())
      // .then(r=>console.log("r : "+r))
      .then(window.location.href = "../html/voldemort.html"))
    }

  }

}

function majSession(nom,valeur){
  var data = new FormData();
  data.append(nom,valeur);

  return fetch("../php/majSession.php",{
    method : 'post',
    body : data
  })

  // .then(r=>console.log("majSession"))
}

function infanticide(pere){
  // fontion permettant de supprimer tous les enfants d'une balise html
  //
  // param pere : balise html dont on veut supprimer les enfants
  // type pere : html object
  // console.log("infanticide");
  while(pere.firstElementChild) {
    pere.removeChild(pere.firstElementChild);
  }
}



function initFigure(){
  //On instancie les figurines de notre jeu

  //Terrier
  terrier = new Figurine("terrier","50.756072","-3.285530","../../public/img/lieu/terrier.png",false,false,"",false,"qcm");
  //Journal de Tom Jedusor
  journal = new Figurine("journal","50.755898","-3.286422","../../public/img/loot/journal80.gif",true,false,"",false,"");
  //hiboux
  hiboux = new Figurine("hiboux","50.756470","-3.286544","../../public/img/lieu/hiboux.gif",false,true,"",false,"journal");
  //Bague des Gaunt
  bague = new Figurine("bague","51.359682","-1.870227","../../public/img/loot/bague80.gif",true,true,"",false,"journal");
  //memo
  memo = new Figurine("memo","51.360277","-1.870581","../../public/img/lieu/memo.gif",false,true,"",false,"bague");
  //Downing street Dumbledore
  dumbledore = new Figurine("dumbledore","51.503624","-0.127593","../../public/img/lieu/dumbledore.png",false,true,"",false,"bague");
  //Dumbledore_bis
  dumbledore_bis = new Figurine("dumbledore_bis","54.397515","-0.475738","../../public/img/lieu/dumbledore.png",false,true,"",false,"dumbledore");
  //Grotte Médaillon
  medaillon = new Figurine("medaillon","54.398877","-0.477079","../../public/img/loot/medaillon80.gif",true,true,"",false,"dumbledore_bis");
  //Dobby
  dobby = new Figurine("dobby","54.399545","-0.481060","../../public/img/lieu/dobby.gif",false,true,"../html/dicotomie.html",false,"medaillon");
  //Gringotts
  gringotts = new Figurine("gringotts","51.514055","-0.088032","../../public/img/lieu/gringotts.gif",false,true,"../html/coupe.html",false,"dobby");
  //Coupe
  coupe = new Figurine("coupe","51.513876","-0.088719","../../public/img/loot/poufsouffle80.gif",true,true,"",false,"gringotts");
  //Weasleys
  weasley = new Figurine("weasley","51.510278","-0.133874","../../public/img/lieu/weasleys.png",false,true,"../html/memory.html",false,"coupe");
  //Diademe
  diadem = new Figurine("diadem","55.415828","-1.706490","../../public/img/loot/diademe80.gif",true,true,"",false,"coupe");
  //Boule Trelawney
  boule = new Figurine("boule","55.415606","-1.704877","../../public/img/lieu/boule.gif",false,true,"",false,"diadem");
  //Manoir
  drago = new Figurine("drago","51.039191","-1.757196","../../public/img/lieu/drago.png",false,true,"../html/labyrinthe.html",false,"diadem");
  //Nagini
  nagini = new Figurine("nagini","51.038535","-1.756981","../../public/img/loot/Nagini80.gif",true,false,"",false,"drago");
  //Rogue
  dumbledore_ter = new Figurine("dumbledore_ter","51.039035","-1.758035","../../public/img/lieu/dumbledore.png",false,true,"",false,"nagini");
  //Harry
  harry = new Figurine("harry","51.691165","-0.416988","../../public/img/harry/harry.gif",true,true,"",false,"nagini");
  //Square Grimmauld
  sirius = new Figurine("sirius","51.531492","-0.110754","../../public/img/harry/sirius.gif",true,true,"",false,"nagini");

  listFigure.push(terrier,journal,bague,dumbledore,medaillon,gringotts,coupe,weasley,diadem,drago,nagini,dumbledore_ter,hiboux,memo,dumbledore_bis,dobby,boule,sirius,harry);

  // console.log("premiere initialisation");

}

function initMap(){
  //On utilise comme fond de carte la couche satellitaire d'ESRI
  // Esri_WorldImagery = L.tileLayer('https://server.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer/tile/{z}/{y}/{x}', {
  //   attribution: 'Tiles &copy; Esri &mdash; Source: Esri, i-cubed, USDA, USGS, AEX, GeoEye, Getmapping, Aerogrid, IGN, IGP, UPR-EGP, and the GIS User Community'
  // });

  // osm_plan = L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
  //   //attribution permet de donner les sources leaflet et openstreetmap
  //   attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
  // });

//   var Stamen_Toner = L.tileLayer('https://stamen-tiles-{s}.a.ssl.fastly.net/toner/{z}/{x}/{y}{r}.{ext}', {
// 	attribution: 'Map tiles by <a href="http://stamen.com">Stamen Design</a>, <a href="http://creativecommons.org/licenses/by/3.0">CC BY 3.0</a> &mdash; Map data &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors',
// 	subdomains: 'abcd',
// 	minZoom: 0,
// 	maxZoom: 20,
// 	ext: 'png'
// });

var CartoDB_Positron = L.tileLayer('https://{s}.basemaps.cartocdn.com/light_all/{z}/{x}/{y}{r}.png', {
	attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors &copy; <a href="https://carto.com/attributions">CARTO</a>',
	subdomains: 'abcd',
	maxZoom: 19
});

  //Création et centrage de la carte
  map = L.map('mapid',{
    layers: [CartoDB_Positron]
  }).setView([53.0,-0.0], 6); //centre de l'image et échelle

  // couches = {
  //   "CartoDB":CartoDB_Positron,
  //   // "Esri": Esri_WorldImagery,
  //   // "OSM": osm_plan
  //   // "Stamen":Stamen_Toner,
  //
  //
  // };


  // multi_couche = L.control.layers(couches).addTo(map);


  L.Control.Valide = L.Control.extend({
      onAdd: function(map) {
          var but = L.DomUtil.create('button');
          but.innerHTML="Rechercher";
          L.DomEvent.on(but,"click",function(event){
            zoomCoord();
          })
          return but;
      },

      onRemove: function(map) {
          // Nothing to do here
      }
  });

  L.control.valide = function(opts) {
      return new L.Control.Valide(opts);
  }

  L.control.valide({ position: 'bottomleft' }).addTo(map);

  L.Control.Longitude = L.Control.extend({
      onAdd: function(map) {
          var lng = L.DomUtil.create('input');
          lng.type='text';
          lng.name="recherche";
          lng.id="longitude";
          lng.placeholder="longitude : 0.0";
          return lng;
      },

      onRemove: function(map) {
          // Nothing to do here
      }
  });

  L.control.longitude = function(opts) {
      return new L.Control.Longitude(opts);
  }

  L.control.longitude({ position: 'bottomleft' }).addTo(map);

  L.Control.Latitude = L.Control.extend({
      onAdd: function(map) {
          var lat = L.DomUtil.create('input');
          lat.type='text';
          lat.name="recherche";
          lat.id="latitude";
          lat.placeholder="latitude : 53.0";
          return lat;
      },

      onRemove: function(map) {
          // Nothing to do here
      }
  });

  L.control.latitude = function(opts) {
      return new L.Control.Latitude(opts);
  }

  L.control.latitude({ position: 'bottomleft' }).addTo(map);


  function zoomCoord(){
  var lat = document.getElementById("latitude").value;
  var long = document.getElementById("longitude").value;
  map.clearLayers;
  map.flyTo(new L.LatLng(lat, long),16);
}

// onKeyPress="if(event.keyCode == 13) zoomCoord()";

document.onkeydown = function(event){
  if (event.keyCode == 13){
    zoomCoord();
  }
}



  //On ajoute un écouteur d'événement permettant de rendre visible les figurines à partir d'une certaine échelle
  map.on("zoom",function(e){
    //si l'on est dans un niveau de zoom supérieur à 10 et que la figure n'est pas bloqué, on dévoile la figure
    if (map.getZoom()>10) {

      for (var i = 0; i < listMarker.length; i++) {
        // if (listFigure[i].bloque==false) {
          // console.log("affiche");
          listMarker[i].setOpacity(1);
        // }
      }
    }
    //si l'on est dans un niveau de zoom inférieur à 11, on cache les figures
    else{
      for (var i = 0; i < listMarker.length; i++) {
        // console.log("affcihe pas")
        if (listMarker[i].id=="harry") {
          if (listMarker[i]>8) {
            listMarker[i].setOpacity(1);
          }
        }
        listMarker[i].setOpacity(0);
      }
    }



  });

  map.on("moveend",function(e){
    // console.log(map.getCenter());
    majSession("coord",map.getCenter());
  })

  // console.log("initialisation map");
}

function afficheLoot(){
  var tr = document.getElementById("lignetab");
  for(let fig in listDetruit){
    // console.log("boucle");
    if (listDetruit[fig].horcruxe){
      if (listDetruit[fig].detruit){
        var td = document.getElementById(listDetruit[fig].nom);
        if (!td.firstElementChild){
          var img = document.createElement("img");
          img.src=listDetruit[fig].url;
          td.appendChild(img);
          tr.appendChild(td);
        }
      }
    }
  }


  // console.log("affiche loot");
  // var tr = document.getElementById("lignetab");
  // var td = document.getElementById(figure.nom);
  // if (!td.firstElementChild){
  //   var img = document.createElement("img");
  //   img.src=figure.url;
  //   td.appendChild(img);
  //   tr.appendChild(td);
  // }

}

function debloqueSuite(figurine){
  // console.log("debloque suite");
  for (let fig in listFigure){
    if(listFigure[fig].clef == figurine.nom){
      console.log(listFigure[fig]);
      console.log(figurine.nom);
      listFigure[fig].bloque=false;
    }
  }

}

function majFigurines(session){
  for (let nom in session){
    console.log(nom);
    let chaine = nom.split('_');
    for (let fig in listFigure){
      if (chaine[0]==listFigure[fig].nom){
        if (chaine[1]=="bloque"){
          console.log(listFigure[fig]);
          listFigure[fig].bloque=session[nom];
        }
        if (chaine[1]=="detruit"){
          listFigure[fig].detruit=session[nom];
        }
      }
    }
  }
  // console.log("maj figurine");
}



function detruireMarker(marker){
  ///Fonction permettant de réaliser la destruction de l'horcruxe. Au niveau fonctionnel, elle retire
  ///le marqueur de la carte et ajoute l'image de l'horcruxe dans le tableau des horcruxes détruits
  ///
  ///param marker : le marqueur sélectionné
  ///type marker : marker leaflet
  ///no return

  marker.remove();
  // console.log("marker detruit");
}

function ajoutMarkerCarte(figure){
  //Fonction permettant de charger une figurine dans la carte c'est à dire de lui attribuer un marqueur
  //
  //param figure : la figure à ajouter à la carte
  //type figure : objet Figurine à ajouter à la carte
  //no return

  //On attribut une image à notre marqueur, de taille 100*100
  // console.log("marker ajoute");
  // console.log(figure.nom);
  var iconFigure = L.icon({
    iconUrl : figure.url,
    iconSize : [100,100],
    popupAnchor:[-50,-50]
  });

  // console.log(figure);


  //On créé un marqueur correspondant à notre figure
  var marker = L.marker([figure.lat,figure.long], {icon: iconFigure, opacity: 0, id: figure.nom});

  //Ajout d'un écouteur d'événement permettant de détruire un horcruxe si on le sélectionne
  if (figure.horcruxe){ //&& (figure.bloque==false)) {
    marker.on("click",function(e){
      figure.detruit=true;
      detruireMarker(e.target);
      var tmp = figure.nom+"_detruit";

      // majSession(tmp,true);
      var data = new FormData();
      data.append(tmp,true);

      fetch("../php/majSession.php",{
        method : 'post',
        body : data
      })
      debloqueSuite(figure);
      chargerSession2();

    })
  }



  //ajout d'un écouteur d'événement qui permet d'entrer dans un mini-jeu pour les marqueurs ayant cette fonctionnalité
  if (figure.ouverture.length>1) {
    marker.on("click",function(){
      var tmp = figure.nom+"_bloque";

      // majSession(tmp,true);
      var data = new FormData();
      data.append(tmp,false);

      fetch("../php/majSession.php",{
        method : 'post',
        body : data
      })
      // debloqueSuite(figure);
      // chargerSession2();
      .then(window.location.href = figure.ouverture)


    })
  }


  if (figure.nom=="hiboux") {
    // console.log('ajout hibou');
    var lieu_crypte = encode_lettre("Cimetière de Stanton St Bernard");
    var lat_crypte_entier = decode_lettre("5.1");
    var lat_crypte_dec = decode_lettre("3.5");
    var long_crypte_entier = decode_lettre("1");
    var long_crypte_dec = decode_lettre("8.7");
    var lettre = L.popup().setContent('<p>Albus, </br> Il semblerait que la relique du cadet se trouve dans la bague que possèdent toujours ces derniers héritiers. </br> Je l\'ai identifié au lieu crypté suivant : </p><ul><li>latitude : '+lat_crypte_entier+','+lat_crypte_dec+'</li><li>longitude : -'+long_crypte_entier+','+long_crypte_dec+'</li><li>lieu : '+lieu_crypte+'</li></ul></br><p>G.Grindelwald</p>');
    marker.on("click",function(){
      marker.bindPopup(lettre).openPopup;
      // debloqueSuite(figure);
      // chargerSession2();
    })
  }


  if (figure.nom=="memo") {
    // console.log('ajout memo');
    var relique = "                    /|\\"+"\n                    / | \\"+"\n                   / _|_ \\"+"\n                  / | | | \\"+"\n                 /  |_|_|  \\"+"\n                /     |     \\"+"\n               /_ _ _ | _ _ _\\\n";
    var memo = L.popup().setContent('<pre>Viens me retrouver au Ministère de la Magie </br></br> '+relique+'</pre>');
    marker.on("click",function(){
      marker.bindPopup(memo).openPopup;
      // debloqueSuite(figure);
      // chargerSession2();
    })
  }

  if (figure.nom=="dumbledore") {
    // console.log('ajout dumbledore');
    marker.on("click",function(){
      map.flyTo(new L.LatLng("54.398877","-0.477079"),16);
      var tmp = figure.nom+"_bloque";

      // majSession(tmp,true);
      var data = new FormData();
      data.append(tmp,false);

      fetch("../php/majSession.php",{
        method : 'post',
        body : data
      })
      debloqueSuite(figure);
      chargerSession2();

    })
  }

  if (figure.nom=="dumbledore_bis") {
    // console.log("ajout dumbledore bis");
    var message = L.popup().setContent('<p>C\'est cela, tu viens de transplaner, et de façon magistrale devrais-je dire, la plupart des gens vomissent la première fois</p>');
    // console.log(message);
    marker.on("click",function(){
      marker.bindPopup(message).openPopup;
      var tmp = figure.nom+"_bloque";

      // majSession(tmp,true);
      var data = new FormData();
      data.append(tmp,false);

      fetch("../php/majSession.php",{
        method : 'post',
        body : data
      })
      debloqueSuite(figure)
      chargerSession2()
      // debloqueSuite(figure);
      // chargerSession2();
    })
  }

  if (figure.nom=="dumbledore_ter") {
    // console.log("ajout dumbledore ter");

    var chaine1 = "<p>Le soir où Lord Voldemort s\'est rendu à Godrics Hollow pour tuer Harry, lorsque Lilly s\'est jetée entre eux pour faire bouclier, le sortilège a ricoché. A cette instant, un fragment de l\'âme de Voldemort s\'est détaché et il s\'est accroché au seul être vivant encore présent qu\'il a trouvé : Harry lui-même. Ooooh ce n\'est pas par hasard qu'il sait parler au serpent, ce n\'est pas par hasard qu\'il peut voir dans l\'esprit de Lord Voldemort. Une";
    var chaine2 = "partie de Voldemort vie à l\'intérieur de lui. Oui, il doit mourir, et vous devez l\'arêter avant qu\'il n\'arrive à rejoindre Sirius Black au square Grimmauld</p>"
    var chainedot = chaine1+chaine2
    var pensine = L.popup().setContent(chainedot);
    // console.log(pensine);
    marker.on("click",function(){
      marker.bindPopup(pensine).openPopup;
      // debloqueSuite(figure);
      // chargerSession2();
    })
  }



  if (figure.nom=="boule") {
    // console.log("ajout boule");
    var lieu_crypte_bis = encode_lettre("Manoir des Malefoy dans le Sud du Wiltshire");
    var lat_crypte_entier_bis = "4*lat_entier - 34 = 170";
    var lat_crypte_dec_bis = "lat_dec = 39/1000";
    var long_crypte_entier_bis = "long_entier = 3 étages sous le 2ème étage";
    var long_crypte_dec_bis = "long_dec = 3157[800]";

    var prophetie = L.popup().setContent('<p>Le prochain horcruxe se trouve au quartier général des mangemorts, à l\'adresse cryptée suivante : <ul><li>'+lat_crypte_entier_bis+'</li><li>'+lat_crypte_dec_bis+'</li><li>'+long_crypte_entier_bis+'</li><li>'+long_crypte_dec_bis+'</li><li>lieu : '+lieu_crypte_bis+'</li></ul>');
    marker.on("click",function(){
      marker.bindPopup(prophetie).openPopup;
      // debloqueSuite(figure);
      // chargerSession2();
    })

  }

  if (figure.nom=="harry") {
    // marker.setTooltipContent("harry");
    // console.log("figure : harry");
    if (temps_harry==null){
      var date_harry = new Date();
      majSession("temps_harry",date_harry.getTime());
      chargerSession();
    }

    deplacerHarry(figure);
    marker.on("click",function(){
      chargerSession();
      var date = new Date();
      var tps = 1260000-(date.getTime()-parseInt(temps));
      // console.log("tps : " + tps);
      var data = new FormData();
      data.append("temps-restant",tps);
      fetch("../php/majSession.php",{
        method : 'post',
        body : data
      })
      // window.location.href = "../php/fin.php";
      // fetch("../php/fin.php")
        // .then(r=>r.text())
        // .then(r=>console.log("r : "+r))
      .then(window.location.href = "../php/fin.php")
    })

  }


  marker.on("add",function(){
    // console.log("entré marker.add");
    // console.log("zoom carte : " + map.getZoom);
    if (figure.nom=="harry") {
      if(map.getZoom()>6){
        marker.setOpacity(1);
      }
      else{
        marker.setOpacity(0);
      }
    }
    else{
      if(map.getZoom()>10) {
        marker.setOpacity(1);
      }
      else{
        marker.setOpacity(0);
      }
    }
  })


  //On ajoute le marqueur à la carte et dans une liste
  marker.addTo(map);
  listMarker.push(marker);

}

function chargement2(){
  //Fonction permettant de charger l'ensemble des figurines de l'escape game
  ///
  ///no return
  // console.log("chargement2");
  listMarker=[];
  for (let i=0;i<listFigure.length;i++){
    if (listFigure[i].detruit==false){
      if(listFigure[i].bloque==false){
        ajoutMarkerCarte(listFigure[i]);
      }
    }
    else{
      listDetruit.push(listFigure.splice(i,1)[0]);
      i-=1;
    }
  }
  // console.log("listMarker");
  // console.log(listMarker);
}

// async function chargerSession2(){
//   const result = await chargerSession();
//   const r = await result.json();
//   majFigurines(r);
//   chargement2();
//   afficheLoot();
// }

function chargerSession2(){
  // console.log("chargerSession2");
  fetch("../php/session.php")
  .then(r=>r.json())
  .then(r=>{
    majFigurines(r);
    chargement2();
    afficheLoot();
  })
}

function deplacerHarry(harry){
  ///
  ///
  ///harry : figurine de Harry
  // console.log("entré dans deplacerHarry(harry)");
  setTimeout(function(){majHarry(harry)},10);

}

function majHarry(harry){
  ///
  ///
  ///harry : figurine de Harry

  // console.log("entré dans majharry(harry)");
  var lat = parseFloat(harry.lat);
  var long = parseFloat(harry.long);
  detruireMarker(listMarker[listMarker.length-1]);
  lat = (lat + delta[0]/60).toString();
  long = (long + delta[1]/60).toString();
  var dlat = delta[0]/60;
  var dlong = delta[1]/60;

  harry.lat = lat;
  harry.long = long;
  // harry.style.transform = "translate("+dlat+"px,"+dlong+"py)";
  // harry.setLatLng(L.latLng(lat,long));
  chargerSession2();
  // if ((harry.lat<=sirius.lat) && (harry.long>=sirius.long)) {
  //   window.location.href="../html/voldemort.html";
  // }


}

//variables de la carte
var Esri_WorldImagery=null;
var osm_plan=null;
var map=null;
var couches=null;
var multi_couche=null;


//variables des figures;
var listFigure = [];
var boule=null;
var dobby=null;
var dumbledore_bis=null;
var dumbledore_ter=null;
var memo=null;
var hiboux=null;
var nagini=null;
var drago=null;
var diadem=null;
var weasley=null;
var coupe=null;
var gringotts=null;
var medaillon=null;
var dumbledore=null;
var bague=null;
var journal=null;
var terrier=null;
var harry=null;
var sirius=null;

var listMarker=[];
var listDetruit=[];
var coord="";

var temps=null;
var temps_harry=null;

// var bouton = document.getElementById("button");
// bouton.addEventListener("click",function(e){
//   var lat = document.getElementById("latitude").value;
//   var long = document.getElementById("longitude").value;
//   map.clearLayers;
//   map.flyTo(new L.LatLng(lat, long),16);
// })

initFigure();

//vecteur Privet Drive - Square Grimmauld
var delta = [];
delta[0] = sirius.lat-harry.lat;
delta[1] = sirius.long-harry.long;

initMap();
chargerSession();
afficheMinuteur();
chargerSession2();
