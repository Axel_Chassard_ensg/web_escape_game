var phrase = "Salut toi";
var dec =10;


//fonction qui permet de faire un modulo d'un nombre negatif
// car % d'un nombre negatif renvoie un modula negatif
Number.prototype.mod = function(n) {
	var m = (( this % n) + n) % n;
	return m < 0 ? m + Math.abs(n) : m;
};


// fonction ayant pour but de crypter une chaine de caractere
// conversion de chaque lettre en chiffre representant sa place dans l'alphabet
// a=1,b=2 etc z=26
//sortie de la forme Bonjour = 2.15.14.10.15.21.18
export function encode_lettre(phrase,decalage=0){
  // converti ttes les lettres en minuscules
  phrase = phrase.toLowerCase();
  //sepapre la phrase en mots
  var mots = phrase.split(' ');
  var code = "";
  // parcourt les mots
  for (let i = 0; i < mots.length ; i++){
    // separe les mots en lettres
    var cars = mots[i].split('');
    // parcourt les lettres
    for (let j = 0; j < cars.length ; j++){
      // code chaque lettre en int en passant par le code ascii
      var a =cars[j].charCodeAt(0);
      //codage du caractere
      code += (cars[j].charCodeAt(0)-97+decalage).mod(26)+1;
      // verifie que ce n est pas le dernier caractere du mot
      if (j < cars.length-1){
        //caractere delimiter entre chaque code de lettre
        code += ".";
      }
    }
    //verifie que ce n est pas le dernier mot
    if (i < mots.length-1){
      //ajout d espace entre chaque code representant un mot
      code += " ";
    }
  }
  return code;
}



//fonction ayant pour but de convertir chaque chiffre en sa lettre correspondante dans l'alphabet
// 0=a,1=b etc 26=z
// Sortie de la forme 2.15.14.10.15.21.18 =Bonjour
export function decode_lettre(code,decalage=0){
  var phrase = "";
  //separation du code via le separateur " " qui permet de distinguer 2 mots
  var nombres = code.split(" ");
  //parcourt l'ensemble de nombres representant les mots
  for (let i = 0; i < nombres.length; i++){
    //separation des nombres pour obtenir via le separateur "." qui permet de distinguer 2 caracteres
    var chiffres = nombres[i].split(".");
    //parcourt des caracteres representes par des nombres
    for (let j = 0; j < chiffres.length; j++){
        // conversion de chiffres en lettre en convertissant chaque chiffre en base 10 puis en passant par le code ascii pour enfin retrouver le caractere correspondant*
        phrase += String.fromCharCode((parseInt(chiffres[j],10)-1-decalage).mod(26)+97);
    }
    //verifie si on arrive au dernier mot de la phrase
    if (i < nombres.length -1){
      phrase += " ";
    }
  }
  return phrase;
}
//
// var crypte = encode_lettre(phrase);
// var decrypte = decode_lettre(crypte);
// console.log(crypte + " = " + decrypte);
